/**
 * @file
 * ClickSpark.js.
 */

(function($) {
  Drupal.behaviors.Clicksparkjs = {
    attach: function(context, settings) {
      $('body').clickSpark({
        'particleImagePath': Drupal.settings.clickspark_js.clickspark_particle_image_path,
        'particleCount': Drupal.settings.clickspark_js.clickspark_particle_count,
        'particleSpeed': Drupal.settings.clickspark_js.clickspark_particle_speed,
        'particleSize': Drupal.settings.clickspark_js.clickspark_particle_size,
        'particleRotationSpeed': Drupal.settings.clickspark_js.clickspark_particle_rotation_speed,
        'animationType': Drupal.settings.clickspark_js.clickspark_animation_type
      });
    }
  }
})(jQuery);
