-- ClickSpark.js --

-- SUMMARY --

Clickspark.js module helps to integrate cliclspark jquery plugin with Drupal
website without any programming knowldge it can be done very easily by
using this module.

-- INSTALLTION --

1) Copy clickspark directory to your modules directory.
2) Download Clickspark.js library from
   https://github.com/ymc-thzi/clickspark.js/archive/master.zip
   and put it in libraries folder. 
3) Clickspark.js library folder name should be 'clickspark.js'.
4) Enable the module at module configuration page.
5) Configure Clickspark.js in admin/config/clickspar/settings page.

-- FEATURES --

1) Easy integration with Clickspark.js plugin.
2) Detailed configuration page to manage different attributes of
   Clickspark.js plugin.
