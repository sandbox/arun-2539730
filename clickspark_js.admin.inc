<?php
/**
 * @file
 * ClickSpark.js admin configurations.
 */

/**
 * ClickSpark.js settings.
 */
function clickspark_js_settings() {
  $form['clickspark_js_particle_count'] = array(
    '#type' => 'textfield',
    '#title' => 'Particle Count',
    '#default_value' => variable_get('clickspark_js_particle_count', 35),
  );
  $form['clickspark_js_particle_speed'] = array(
    '#type' => 'textfield',
    '#title' => 'Particle Speed',
    '#default_value' => variable_get('clickspark_js_particle_speed', 12),
  );
  $form['clickspark_js_particle_size'] = array(
    '#type' => 'textfield',
    '#title' => 'Particle Size',
    '#default_value' => variable_get('clickspark_js_particle_size', 12),
  );
  $form['clickspark_js_particle_rotation_speed'] = array(
    '#type' => 'textfield',
    '#title' => 'Particle Rotation Speed',
    '#default_value' => variable_get('clickspark_js_particle_rotation_speed', 0),
  );
  $form['clickspark_js_animation_type'] = array(
    '#type' => 'select',
    '#title' => 'Animation Type',
    '#options' => array(
      'explosion' => t('Explosion'),
      'splash' => t('Splash'),
      'falloff' => t('Falloff'),
      'blowright' => t('Blowright'),
      'blowleft' => t('Blowleft'),
    ),
    '#default_value' => variable_get('clickspark_js_animation_type', ''),
  );
  // If there is already an uploaded image display the image here.
  if ($image_fid = variable_get('clickspark_js_particle_image_path', FALSE)) {
    $image = file_load($image_fid);
    $form['image'] = array(
      '#markup' => theme('image_style', array(
        'style_name' => 'thumbnail',
        'path' => $image->uri,
        'width' => '',
        'height' => '',
      )),
      '#prefix' => '<label>Preview</label>',
    );
  }
  $form['clickspark_js_particle_image_path_file'] = array(
    '#type' => 'managed_file',
    '#name' => 'clickspark_js_particle_image_path_file',
    '#title' => t('Particle Image'),
    '#upload_location' => 'public://clickspark_js_particle_image/',
    '#process' => array('_clickspark_js_particle_image_path_element_process'),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      // Pass the maximum file size in bytes.
      'file_validate_size' => array('MAX_FILE_SIZE' * 1024 * 1024),
    ),
  );
  // Page specific visibility configurations.
  $php_access = user_access('use text format php_code');
  $visibility = variable_get('clickspark_js_visibility_options', 0);
  $pages = variable_get('clickspark_js_visibility_pages', 'admin/*');
  $options = array();
  $title = '';
  $description = '';
  if ($visibility == 2 && !$php_access) {
    $form['clickspark_js_visibility_options'] = array('#type' => 'value', '#value' => 1);
    $form['clickspark_js_visibility_pages'] = array('#type' => 'value', '#value' => $pages);
  }
  else {
    $options = array(
      t('Every page except the listed pages'),
      t('The listed pages only'),
    );
    $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array(
      '%blog' => 'blog',
      '%blog-wildcard' => 'blog/*',
      '%front' => '<front>',
    ));
    if (module_exists('php') && $php_access) {
      $options[] = t('Pages on which this PHP code returns <code>TRUE</code> (experts only)');
      $title = t('Pages or PHP code');
      $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    else {
      $title = t('Pages');
    }
  }
  $form['clickspark_js_visibility_options'] = array(
    '#type' => 'radios',
    '#title' => t('Add ClickSpark to specific pages'),
    '#options' => $options,
    '#default_value' => variable_get('clickspark_js_visibility_options', 0),
  );
  $form['clickspark_js_visibility_pages'] = array(
    '#type' => 'textarea',
    '#title' => $title,
    '#default_value' => $pages,
    '#description' => $description,
    '#wysiwyg' => FALSE,
    '#rows' => 10,
  );
  $form['#validate'][] = 'clickspark_js_particle_image_path_form_validate';
  $form['#submit'][] = 'clickspark_js_particle_image_path_form_submit';
  return system_settings_form($form);
}
/**
 * Function to hide the upload button of managed file field.
 */
function _clickspark_js_particle_image_path_element_process($element, &$form_state, $form) {
  $element = file_managed_file_process($element, $form_state, $form);
  $element['upload_button']['#access'] = FALSE;
  return $element;
}
/**
 * Function to validate particle image diamention.
 */
function clickspark_js_particle_image_path_form_validate($form, $form_state) {
  $file = $form['clickspark_js_particle_image_path_file']['#file'];
  if (isset($file->uri)) {
    $image_info = image_get_info($file->uri);
    if ($image_info['width'] > 25 || $image_info['height'] > 25) {
      form_set_error('clickspark_js_particle_image_path_file', t('The image is too large; the maximum dimensions are %dimensions pixels.', array('%dimensions' => '25x25')));
    }
  }
}
/**
 * Function to save the image.
 */
function clickspark_js_particle_image_path_form_submit($form, $form_state) {
  if (!empty($form['clickspark_js_particle_image_path_file']['#file'])) {
    $form['clickspark_js_particle_image_path_file']['#file']->status = FILE_STATUS_PERMANENT;
    file_save($form['clickspark_js_particle_image_path_file']['#file']);
    variable_set('clickspark_js_particle_image_path', $form['clickspark_js_particle_image_path_file']['#file']->fid);
  }
}
